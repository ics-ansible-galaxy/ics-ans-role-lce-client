# ics-ans-role-lce-client

Ansible role to install lce-client.

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-lce-client
```

## License

BSD 2-clause
