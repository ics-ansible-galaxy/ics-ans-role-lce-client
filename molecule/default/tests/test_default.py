import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_lce_client_installed(host):
    lce_client = host.package("lce_client")
    assert lce_client.is_installed


def test_lceclient_is_running_and_enabled(host):
    lce_client = host.service("lce_client")
    assert lce_client.is_enabled
    assert lce_client.is_running
